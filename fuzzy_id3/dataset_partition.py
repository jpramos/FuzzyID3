
import pandas as pd
import numpy as np
import data_parser as dp
import math
import sys

#DATASET = 'infarctionA'
DATASET = 'arrhythmia'
MINITEMS = 2

#def instance_class(instance):
#
#    return int(instance[-1])
#
#def find_max_class(dta_classes):
#
#    dta_classes = map(int, dta_classes)
#
#    return max(list(set(dta_classes)))
#
#
#def find_all_freq(dta_instances, max_class):
#
#    class_freq = [0 for x in range(max_class + 1)]
#
#    for rec in dta_instances:
#        class_freq[instance_class(rec)] += 1
#
#    return class_freq


def entropy(freqs):

    p = freqs / freqs.sum()
    partial = p * np.log2(p)
    entrop = partial.sum() * -1

    return entrop
    #for v in range(len(freqs)):
    #    if freqs[v] != 0:
    #        ent += freqs[v]/total_cases * math.log(freqs[v]/total_cases, 2)

    #return -ent


def split_set(dta_instances, cut_points):

    limits = (0,) + tuple(boundary + 1 for boundary in cut_points) + (len(dta_instances)+1, )


    subset = []
    for start, end in zip(limits, limits[1:]):
        subset.append(dta_instances[start:end])

    return subset

def info_gain(dta_instances, cut_points):

    subsets = split_set(dta_instances, cut_points)

    #whole_freq = find_all_freq(dta_instances, max_class)
    total_cases = float(len(dta_instances))
    whole_freq = dta_instances.groupby('class').size()

    (subset_freq, subset_len) = zip(*[(set.groupby('class').size(),
        len(set)) for set in subsets])

    #(subset_freq, subset_len) = zip(*[(find_all_freq(set, max_class), len(set))
        #for set in subsets])

    whole_entropy = entropy(whole_freq)
    subset_entropy = [entropy(freq) for freq in subset_freq]
    #print whole_entropy
    #print subset_entropy

    infoX = 0
    splitInfo = 0
    for (set_entropy, set_cases) in zip(subset_entropy, subset_len):
        if set_cases == 0 or set_entropy == 0:
            continue
        #if set_cases == 0:
        infoX += (set_cases/total_cases) * set_entropy
        splitInfo -= (set_cases/total_cases) * math.log(set_cases/total_cases,
                2)

    gain = whole_entropy - infoX
    gainR = gain / splitInfo
    #print infoX
    #print gain
    #print gainR

    return gainR


def find_split_points(att_list):

    #att_data = zip(*att_list)[0]

    #points = sorted(list(set(att_data)))
    cpoints = att_list.drop_duplicates(take_last=True, subset=att_list.columns[0])

    #cpoints = [att_data.index(points[p])-1 for p in range(1,len(points))]

    return cpoints.index.values


def attribute_partition(att_list, ncases, split_ratio, att_cut, offset, level):

    if att_list.empty:
        return

    pcases = len(att_list)

    minsplit = max(MINITEMS, min(25, .1 * split_ratio))
    if minsplit > pcases:
        return    

    if level > 1:
        return
    level+=1

    #if len(att_list) < 50:
    #    return
    
    max_igain = 0 #-sys.float_info.max
    best_cutp = 0
    cutpoints = find_split_points(att_list)
    cutpoints = cutpoints[cutpoints > minsplit]
    cutpoints = cutpoints[cutpoints < (pcases - minsplit)]
    for cutp in cutpoints:
        if cutp == len(att_list)-1 or cutp == 0:
            continue
    #for cutp in range(1, len(att_list)-1):
        igain = info_gain(att_list, [cutp])

        #print '------', max_igain, igain


        if igain > max_igain:
            max_igain = igain
            best_cutp = cutp

        #print subset_inf
        #print len(subset_inf)
        #print '-----'
        #print subset_sup
        #print len(subset_sup)
    
    #if min_igain > .1:
    #    return

    if max_igain <= 0:
        return
   # if max_igain == -sys.float_info.max:
   #     return


    #print max_igain
    
    attribute_partition(att_list[:best_cutp], ncases, split_ratio, att_cut, offset, level)

    #save 
    att_cut.extend([offset + best_cutp])
#    print min_igain
#    print offset + best_cutp
#

    attribute_partition(att_list[best_cutp:], ncases, split_ratio, att_cut,
            offset+best_cutp, level)

    return 


def dataset_partition(attributes, curr_attlist, dta_instances):

    #dta_classes = zip(*dta_instances)[-1]
    #dta_classes = map(int, dta_classes)
    #dta_classes = dta_instances.ix[:,-1]

    #max_class = find_max_class(dta_classes)

    #class_freq = find_all_freq(dta_instances, max_class)
    # TODO class str should be a function argument
    class_freq = dta_instances.groupby('class').size()
    ncases = sum(class_freq)
    split_ratio = ncases / len(class_freq)

    #(cpoints, gratio) = zip(*[ ([],0) for _ in range(len(dta_classes))])
    cpoints = [0] * (len(curr_attlist)-1)
    gratio = [0] * (len(curr_attlist)-1)
    #cpoints = [0] * dta_instances.shape[1]
    #gratio = [0] * dta_instances.shape[1]

    #att_data = zip(*dta_instances)

    count = 0
    for catt in curr_attlist:
        if count == len(curr_attlist)-1:
            break

        #orig_ind = attributes.index(catt)
        #att = att_data[orig_ind]

        #att = map(float, att)
        #att_list =  zip(att, dta_classes)
        #att_list.sort(key=lambda att: att[0])
        att_list = dta_instances.ix[:,[catt,
            'class']].sort(catt).reset_index(drop=True)

        #print catt
        #if catt == 'diphasic_R_DI':
            #print 'stop'
#        if catt == 'R_V6':
#            print catt

        att_cut = []
        attribute_partition(att_list, ncases, split_ratio, att_cut, 0, 0)
#print att_cut
        if att_cut:

            
            #att_cut = sorted(list(set(att_cut)))

            cpoints[count] = att_list.ix[att_cut, 0].values
            #att_cpoint = [att_list[cut][0] for cut in att_cut]
            #cpoints[count] = att_cpoint

            #print '----', att_cut, att_cpoint, catt
            att_gratio = info_gain(att_list, att_cut)
            gratio[count] = att_gratio
        else:
            cpoints[count] = []
            gratio[count] = -float('Inf')

        count+=1
        


    return cpoints, gratio, class_freq


def main():

    attr_list = dp.read_names('input_data/' + DATASET + '.names')
    dta_instances = dp.read_data('input_data/' + DATASET + '.data')
    #(dta_instances, dta_class, dta_full) = dp.read_data('data/' + DATASET + '.data')

    dta_set = pd.DataFrame(dta_instances, columns=attr_list)
    dataset_partition(attr_list, attr_list[:], dta_set)



if __name__ == '__main__':


    main()

