
import math

def trimf(x, a, b, c):
    
    #print '-------'
    #try:
    #    print (x-a)/(b-a)
    #    val1 = (x-a)/(b-a)
    #except ZeroDivisionError:
    #    print '0s'
    #    val1 = 0
    #try:
    #    print (c-x)/(c-b)
    #    val2 = (c-x)/(c-b)
    #except ZeroDivisionError:
    #    print '0s'
    #    val2 = 0
    #print '-------'

    if a != b:
        if a < x < b:
            return (x-a)/(b-a)

    if b != c:
        if b < x < c:
            return (c-x)/(c-b)

    if x == b:
        return 1
    else:
        return 0


def gaussmf(x, sig, c):


    if sig == 0:
        if x == c:
            return 1
        else:
            return 0
    else:
        return math.exp(-(x - c)**2 / (2 * sig**2))


def trapmf(x, a, b, c, d):

    y1 = y2 = 0
    if x >= b:
        y1 = 1

    if a != b:
        if a <= x < b:
            y1 = (x - a) / (b - a)


    if x <= c:
        y2 = 1

    if c != d:
        if c < x <= d:
            y2 = (d - x) / (d - c)

    return min(y1, y2)

