
import data_parser as dtap
import dataset_partition as dp
import membership_functions as mfs
import sys

import pandas as pd
import numpy as np

from sklearn.cross_validation import Bootstrap
from sklearn.metrics import f1_score

def new_node(node_class, cases):
    """
    typedef  struct _treerec
         {
            BranchType	NodeType;
            ClassNo	Leaf;		/* best class at this node */
            CaseCount	Cases,		/* no of cases at this node */
                        *ClassDist,	/* class distribution of cases */
                        Errors;		/* est or resub errors at this node */
            Attribute	Tested; 	/* attribute referenced in test */
            int		Forks,		/* number of branches at this node */
                        Leaves;		/* number of non-empty leaves in tree */
            ContValue	Cut,		/* threshold for continuous attribute */
                        Lower,		/* lower limit of soft threshold */
                        Upper,		/* upper limit ditto */
                        Mid;		/* midpoint for soft threshold */
            Set         *Subset;	/* subsets of discrete values  */
            Tree	*Branch,	/* Branch[x] = subtree for outcome x */
                        Parent;		/* node above this one */
         }
         TreeRec;
    """

    return {'node_type': 0, 'leaf': node_class, 'cases': cases, 'attribute':
            [], 'cut': [], 'forks': 0, 'mfs': [], 'branch': [], 'parent': [],
            'att_ind': []}


def choose_attribute(current_attlist, gRatio):

    att_ind = gRatio.index(max(gRatio))
    att = current_attlist[att_ind]
    #original_attind = attributes.index(att)

    return att_ind, att


def get_data(data, att_name, val_minbound, val_maxbound):

    data = data.copy(True)
    data = data[data[att_name] >= val_minbound]
    data = data[data[att_name] < val_maxbound]

    return data 

#def get_data(data, att_dta, val_minbound, val_maxbound):
#
#    subset = []
#
#    count = 0
#    for val in att_dta:
#        if val_minbound <= val < val_maxbound:
#            subset.extend([data[count]])
#
#        count += 1
#
#    return subset
#[rec for rec in data if startind <= rec[att_ind] <= endind]

def set_boundaries(att_data, cpoints):

    if min(att_data) == max(att_data):
        return []

    if cpoints[0] == min(att_data):
        limits = [boundary for boundary in cpoints] + [max(att_data)]
        if len(limits) == 2:
            limits = limits + [max(att_data)]
    elif cpoints[0] == max(att_data):
        limits = [max(att_data)] + [boundary for boundary in cpoints]
        if len(limits) == 2:
            limits = [min(att_data)] + limits
    else:
        limits = [min(att_data)] + [boundary for boundary in
            cpoints] + [max(att_data)]

    limits[-1] += .1
    
    #print limits
    return limits


def set_mfs(tree, type=2):
    """
    type = 0 - triangular with its peak on the mode of the sample data
    type = 1 - triangular with its peak on the mid point of the sample data
    type = 2 - gaussian
    type = 3 - trapezoidal
    """

    if not type:
        cut_points = tree['cut']

        children = tree['branch']
        att_ind = tree['attribute']

        mf = []
        firstborn = children[0]
        child_data = map(float, firstborn['cases'][att_ind])
        a = -sys.float_info.max
        #a = min(child_data)-.5
        b = max(set(child_data), key=child_data.count)
        mf.extend([[mfs.trimf, [a, b]]])

        for ch in range(1, len(children)):
            child = children[ch]
            child_data = map(float, child['cases'][att_ind])

            a = b
            b = max(set(child_data), key=child_data.count)
            #c = (cut_points[ch] + b) / 2
            c = b

            mf[ch-1][1] += [c]
            mf.extend([[mfs.trimf, [a, b]]])

        
        mf[-1][1] += [sys.float_info.max]
    elif type == 1:

        cut_points = tree['cut']
        children = tree['branch']
        att_ind = tree['attribute']

        mf = []
        firstborn  =children[0]
        child_data = map(float, firstborn['cases'][att_ind])
        a = -sys.float_info.max
        b = (min(child_data) + max(child_data)) / 2
        mf.extend([[mfs.trimf, [a, b]]])

        for ch in range(1, len(children)):
            child = children[ch]
            child_data = map(float, child['cases'][att_ind])

            #a = (min(child_data) + mf[ch-1][1]) / 2
            a = b
            b = (min(child_data) + max(child_data)) / 2
            #c = (max(child_data) + b) / 2
            c = b

            mf[ch-1][1] += [c]
            mf.extend([[mfs.trimf, [a, b]]])

        mf[-1][1] += [sys.float_info.max]
    elif type == 2:

        att_ind = tree['attribute']

        mf = []
        children = tree['branch']
        for child in children:
            
            child_data = map(float, child['cases'][att_ind])

            length = len(child_data)
            mean_v = sum(child_data) / len(child_data)
            sum2 = sum(x*x for x in child_data)
            std = abs(sum2 / length - mean_v**2)**.5

            mf.extend([[mfs.gaussmf, [std, mean_v]]])

    elif type == 3:

        cut_points = tree['cut']
        att_ind = tree['attribute']
        children = tree['branch']

        mf = []
        firstborn = children[0]
        child_data = map(float, firstborn['cases'][att_ind])
        a = -sys.float_info.max
        b = cut_points[0]
        c = cut_points[1]




    return mf
    

def form_tree(data, current_attlist, rec_level):

    tree = new_node(0, [])
    current_attlist = current_attlist[:]

    if data.empty:
        return tree
    
    #classes = zip(*data)[-1]
    classes = data.ix[:,-1]

    #if classes.count(classes[0]) == len(classes):
    if len(classes[classes == classes.head(1).values[0]]) == len(classes):
        return new_node(classes.head(1).values[0], data)


    (cPoints, gRatio, class_freq) = dp.dataset_partition(current_attlist, current_attlist, data)

    if not cPoints or not gRatio or (gRatio.count(gRatio[0]) == len(gRatio)):
        return new_node(class_freq.idxmax(), data)

    #if not cPoints or not gRatio:
        #print class_freq
    #    return new_node(class_freq.index(max(class_freq)), data)

    #NOTES NOTES NOTES
    # Data has to partitioned in each iteration (remember... divide & conquer)
    att_ind, att_name = choose_attribute(current_attlist, gRatio)

    # get cut points for the current attribute
    #att_cpoints = sorted(list(set(cPoints[att_ind])))
    att_cpoints = cPoints[att_ind]

    if not att_cpoints.size:
        return new_node(class_freq.idxmax(), data)
    
    # get attribute data
    #att_data = map(float, zip(*data)[orig_ind])
    att_data = data.ix[:,att_name]

    
    #print current_attlist[att_ind]
    limits = set_boundaries(att_data, att_cpoints)
    if not limits:
        return new_node(class_freq.idxmax(), data)
    #limits = (min(att_data), ) + tuple(boundary for boundary in
    #        att_cpoints) + (max(att_data), )

    att = current_attlist.pop(att_ind)
    #data = data.drop(att_name, 1)
    #print rec_level, ' ', att, ' ', limits
    for (start, end) in zip(limits, limits[1:]):
        subset = get_data(data, att_name, start, end)
        child = form_tree(subset, current_attlist,rec_level + 1)

        if att_name == 'diphasic_R_DI':
            print 'stop'

        child['parent'] = tree
        tree['branch'].append(child)
        tree['forks'] += 1

    tree['node_type'] = 1
    tree['cut'] = limits
    tree['cases'] = data
    tree['attribute'] = att_name
    tree['mfs'] = set_mfs(tree)


    return tree


def print_tree(tree, level):

    #if not tree['attribute']:
    #    return


    if tree['node_type'] == 1:
        print '|'*level + tree['attribute'], tree['cut']
    else:
        print '|'*level , int(tree['leaf'])

    for child in tree['branch']:
        print_tree(child, level+1)


def construct(data, attributes):

    # partition the dataset
    # should return each attribute partitioned, cut_points and gain ratio
    # (cpoints, gRatio) = dp.dataset_partition(attributes, data)

    dta_set = pd.DataFrame(data, columns=attributes) 
    tree = form_tree(dta_set, attributes, 0)
    
    return tree       

def classify_instance(tree, data, att_list, fuzzy_zadeh, fuzzy_luka, level):

    if tree['node_type'] == 0:
        print ' '*level, 'reached a leaf: ', float(tree['leaf']), fuzzy_zadeh, fuzzy_luka, '\n'
        return tree['leaf'], fuzzy_zadeh, fuzzy_luka

    att = tree['attribute']
    att_ind = att_list.index(att)

    #print '\n', att

    c = 0
    limits = tree['cut']
    clas = []
    zadeh_ret = []
    luka_ret = []
    fuzz_val = []
    for (start, end) in zip(limits, limits[1:]):
        mf_boundaries = tree['mfs'][c]
        #print '-------------', mf_boundaries
        #fuzzy_val = mfs.trimf(float(data[att_ind]), *mf_boundaries)
        fuzzy_val = mf_boundaries[0](float(data[att_ind]), *mf_boundaries[1])
        #print '---', fuzzy_val
        #print '|'*level, att, mf_boundaries, len(tree['branch'][c]['cases'][0]), float(data[att_ind]), fuzzy_zadeh, fuzzy_luka, fuzzy_val, fuzzy_val * (len(tree['branch'][c]['cases'][0])*1.0/len(tree['cases'][0])*1.0), float(data[-1]), start, end, '\n'
        #fuzzy_val *= (len(tree['branch'][c]['cases'][0])*1.0/len(tree['cases'][0])*1.0)
        fuzz_val.extend([fuzzy_val])
        if fuzzy_val > 0:
        #fuzzy_val = 1
        #if start <= float(data[att_ind]) < end:
            cs, fret, ret = classify_instance(tree['branch'][c], data, att_list,
                    min([fuzzy_zadeh, fuzzy_val]), fuzzy_luka * fuzzy_val, level+1)
            clas.extend([cs])
            zadeh_ret.extend([fret])
            luka_ret.extend([ret])
        c += 1

    print '-'*(level+1), att, zadeh_ret, luka_ret, fuzz_val, clas, float(data[-1])
    max_luka = max(luka_ret)
    max_zadeh = max(zadeh_ret)
    #max_class = clas[luka_ret.index(max_luka)]
    max_class = clas[zadeh_ret.index(max_zadeh)]
    #max_fuzzy = max(zadeh_ret)
    #max_class = clas[zadeh_ret.index(max_fuzzy)]
    #print 'fuzzy_class ---', fuzzy_class
    if not clas:
        return 0, max_zadeh, max_luka
    else:
        return max_class, max_zadeh, max_luka

def classify(tree, dataset, att_list):

    count = 0
    y_true = []
    y_pred = []
    ins = 0
    for datum in dataset:
        if ins == 6:
            print 'hey'
        ins += 1
        dtap.print_instance(datum, att_list, 1)
        res, fuzzy_zadeh, fuzzy_luka = classify_instance(tree, datum, att_list, 1, 1, 0)
        print float(datum[-1]), ' ', res
        #print ' %%% NEW INSTANCE %%%'
        y_true.extend([float(datum[-1])])
        y_pred.extend([float(res)])
        if float(datum[-1]) == float(res):
            count += 1
        #print int(res), ' ', int(datum[-1])

    print 'overall: ', count/(len(dataset)*1.0)
    f1s = f1_score(y_true, y_pred)
    print 'individual f1_score: ', f1s

    return f1s

def main():
    
    DATASET = 'arrhythmia'
    #DATASET = 'infarctionA'

    attr_list = dtap.read_names('input_data/' + DATASET + '.names')
    dta_instances = dtap.read_data('input_data/' + DATASET +
            '.data')

    dta_instances = dta_instances[0:100, :]
    tree = construct(dta_instances, attr_list)

    #print_tree(tree, 0)

    classify(tree, dta_instances, attr_list)

def xval():

    #DATASET = 'infarctionA'
    DATASET = 'arrhythmia'

    attr_list = dtap.read_names('input_data/' + DATASET + '.names')
    dta_instances, dta_class, _, dta_full = dtap.read_data('input_data/' + DATASET +
            '.data')

    bs = Bootstrap(len(dta_full), n_iter=5, train_size=.7)
    f1s = []
    for (train_ind, test_ind) in bs:
        train_dta = [dta_full[t] for t in train_ind]
        test_dta = [dta_full[t] for t in test_ind]

        tree = construct(train_dta, attr_list)

        res = classify(tree, test_dta, attr_list)
        f1s.extend([res])


    print 'mean f1_score: ', sum(f1s)/len(f1s)

    return


if __name__ == '__main__':

    main()
    #xval()
