
import numpy as np
import pandas as pd
import pylab as plt

# http://nbviewer.ipython.org/url/cbcb.umd.edu/~hcorrada/PML/src/knn.ipynb#

DATASET = 'arrhythmia'

def main():

    # READ DATASET

    values = np.genfromtxt('input_data/' + DATASET + '.data', delimiter= ',',
            missing_values='?', dtype=float)

    fl = open('input_data/' + DATASET + '.names', 'r')
    lines = [line.strip() for line in fl]
    fl.close()

    colnames = [line.partition(':')[0] for line in lines if not (len(line) == 0 or
        line[0] == '|' or line[0] == '1')]
    del colnames[0]

    # pretify
    values_df = pd.DataFrame(values, columns=colnames)
    #print values_df.ix[:3, -3:]

    # Partition data into train, test
    nsamples = values_df.shape[0]
    ntest = np.floor(.3 * nsamples)

    # Seed the random number generator to be reproducible
    np.random.seed(42)
    all_indices = np.arange(nsamples) + 1
    np.random.shuffle(all_indices)
    test_indices = all_indices[:ntest]
    train_indices = all_indices[ntest:]

    values_train = values_df.ix[train_indices, :]
    values_test = values_df.ix[test_indices, :]

    # save in proc_data
    #pd.save(values_train, 'proc_data/training_data/' + DATASET + '_train.pdat')
    #pd.save(values_test, 'proc_data/test_data/' + DATASET + '_test.pdat')

    # data exploration

    # how many samples and features
    print 'number of features ', len(values_train.columns)
    print 'number of samples ', values_train.shape

    # how many samples we have of each class
    print values_train.groupby('class').size()

    #what is the mean frequency per label
    grouped_dat = values_train.groupby('class').mean()

    plt.figure()
    grouped_dat.ix[:,:10].T.plot(kind='bar')
    plt.show()


if __name__ == '__main__':

    main()
